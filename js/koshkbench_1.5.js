/**************************
 *						*
 *   REQUIRES BOOTSTRAP   *
 *						*
 **************************/

/********************************************************
 *														*
 *   SYS OBJECT DOC:									*
 *														*
 *		locale:	строки, которые должны изменяться		*
 *				в зависимости от языка или приложения	*
 *														*
 *		cfg:											*
 *			dialogHeightMargin:	отступ сверху страницы	*
 *								перед диалоговым окном	*
 *			handleErrors:	будут ли все ошибки			*
 *							выводиться в диалоге вместо	*
 *							консоли						*
 *														*
 *		initializations:	список функций, которые		*
 *							должны быть вызваны после	*
 *							загрузки окна				*
 *														*
 ********************************************************/

HTMLElement.prototype.appendTag = function(tag){
	return this.appendChild(document.createElement(tag))
}
HTMLElement.prototype.prependTag = function(tag){
	return this.insertBefore(document.createElement(tag), this.firstChild)
}
HTMLElement.prototype.appendText = function(text){
	return this.appendChild(document.createTextNode(text))
}
if (!Array.prototype.remove) {
	Array.prototype.remove = function (element) {
		var idx = this.indexOf(element)
		if (idx > -1) {
			this.splice(idx, 1)
		}
		return idx;
	};
}
$.fn.appendText = function (text) {
	return this.each(function () {
		var textNode = document.createTextNode(text);
		this.append(textNode);
	});
};
$.fn.setLines = function (lines) {
	if (!this.length) return this
	if (typeof lines == 'string') {
		lines = lines.split(/\r\n|\n|<br>/)
	}
	this.contents().remove()

	for (var i = 0; i < lines.length; i++) {
		if (i > 0) {
			this.append($('<br>'))
		}
		this.appendText(lines[i])
	}
	return this
};

function LinkedListNode(value) {
	this.value = value
	this.next = null
	this.prev = null
}

function unix_to_date(unix_timestamp, flag) {
	var date = new Date(unix_timestamp * 1000)
	switch (flag) {
		case 'todaytime' : 
			var res = date.toLocaleString('ru-RU').split(', ')
			if (res[0] == TODAY_DATE) {
				return res[1]
			}
			return res[0]
		case 'time' : 
			return date.toLocaleTimeString('ru-RU')
		case 'date' : 
			return date.toLocaleDateString('ru-RU')
		default:
			return date.toLocaleString('ru-RU')
	}
}

function typeCheck(v, t, n) {
	if (typeof v != t) {
		console.error(n + ' is not a ' + t)
		return false
	}
	return true
}

function LinkedList() {
	this.head = null;
	this.tail = null;
	this.addFirst = function(value) {
		var newNode = (value instanceof LinkedListNode) ? value : new LinkedListNode(value)
		if (this.head == null) {
			this.head = newNode
			this.tail = newNode
		}
		else {
			this.head.prev = newNode
			newNode.next = this.head
			this.head = newNode
		}
	}
	this.addLast = this.add = function(value) {
		var newNode = (value instanceof LinkedListNode) ? value : new LinkedListNode(value)
		if (this.head == null) {
			this.head = newNode
			this.tail = newNode
		}
		else {
			this.tail.next = newNode
			newNode.prev = this.tail
			this.tail = newNode
		}
	}
}

var SYS = {
	locale: {}
	, cfg: {
		dialogHeightMargin: 184
		, handleErrors: true
	}
	, initializations: new LinkedList()
}
var BODY, kbMessageBox = false;
var TODAY_DATE = new Date().toLocaleDateString('ru-RU')

SYS.initializations.add(function(){
	var bsTooltips = $('[data-toggle="tooltip"]')
	if (bsTooltips.length) {
		bsTooltips.tooltip()
	}
	
	if (!kbImageOverlay.initialized) {
		initializeImageOverlay()
	}
	$('div[kbSpoiler]').each(function() {
		makeSpoiler(this);
	});
	if (bsTooltips.modal) {
		kbMessageBox = new KBDialog({
			title: "Внимание"
			, id: 'kbMessageBox'
		})
	}

	/*if (SYS.handleErrors) {
		window.onerror = function(msg, url, line, col, error) {
			// Note that col & error are new to the HTML 5 spec and may not be 
			// supported in every browser.
			var extra = !col ? '' : '<br>column: ' + col
			if (error) extra += '<br>error: ' + error
	
			// You can view the information in an alert to see things working like this:
			showMessageBox("Error: " + msg + "<br><br>in " + url + "<br>at line " + line + extra, 'error');
			return true
		}
	}*/
})

$(function () {
	BODY = document.getElementsByTagName('body')[0];
	var initfunc = SYS.initializations.head
	while (initfunc != null) {
		if (typeof initfunc.value == 'function') {
			initfunc.value()
		}
		else {
			console.error('SYS.initializations contains non-functions')
		}
		initfunc = initfunc.next
	}
})

function getRandomInt(min, max) {
	return Math.floor(Math.random() * (max - min)) + min;
}

/*window.addEventListener("resize", function() {
	$('.modal-body').css("max-height", window.innerHeight - SYS.cfg.dialogHeightMargin)
})*/

function KBPoint(itop, ileft) {
	this.top = itop;
	this.left = ileft;
}

function validateElement(element) {
	if (element != undefined) {
		if ((typeof element == 'object') && element instanceof HTMLElement) {
			return element
		} else if (typeof element == 'string') {
			return document.getElementById(element)
		}
	}
	return false
}

function kbException(message) {
	this.message = message
	this.level = 0
	if (arguments.length > 1) {
		if (arguments[1])
			this.level = 1
	}
	this.toString = function() { return this.message; }
}

function processException(e) {
	if (e instanceof kbException) {
		if (e.level)
			console.warn(e.message)
		else
			console.error(e.message)
	} else {
		throw e
	}
}

var kbImageOverlay = {
	initialized: false
	, refreshData: function() {
		alert("Инициализация ещё не завершена, подождите буквально секунду")
	}
	, showImage: function() {
		alert("Инициализация ещё не завершена, подождите буквально секунду")
	}
}

$.fn.KBDropdown = function (params) {
	if (!params) return this
	var items, activeClass, _value, selectedIdx = -1
	var _dropdown = this
	var onChange = []

	if (params.Items) {
		if (typeof params.SelectedIdx == 'number') {
			selectedIdx = params.SelectedIdx
		}
		items = params.Items
	} else {
		items = params
	}
	if (typeof items == 'object' && items instanceof Array && items.length) {
		if (typeof items[0] != 'object') {
			var normalized = []
			for (var i = 0; i < items.length; i++) {
				normalized.push({ Value: items[i] })
			}
			items = normalized
		}

		this.addClass('kbDropdown').css({ position: 'relative' })
		var kbDropdownValue = $('<div class="kbDropdownValue inactive">').appendTo(this).css({ width: '100%', height: '100%' }).click(function() {
			showOptions()
		})
		var kbDropdownOptions = $('<div class="kbDropdownOptions">').appendTo(this).css({ position: 'absolute', zIndex: '1' }).hide()
		
		for (var i = 0; i < items.length; i++) {
			var item = items[i]
			var option = $('<div class="kbDropdownOption">').appendTo(kbDropdownOptions).text(item.Text || item.Value)
			if (item.Class) {
				option.addClass(item.Class)
			}
			option[0]._ValIdx = i
			option.click(function() {
				hideOptions()
				_dropdown.SetValue(this._ValIdx)
			})
		}

		function hideOptions() {
			kbDropdownOptions.hide()
			kbDropdownValue.addClass('inactive')
			kbDropdownValue.removeClass('active')

			document.removeEventListener('click', _ddClick);
		}
		function showOptions() {
			var h = kbDropdownValue.outerHeight()
			kbDropdownOptions.css({ top: h + 'px', left: '0px' })
			kbDropdownOptions.show()
			kbDropdownValue.removeClass('inactive')
			kbDropdownValue.addClass('active')

			document.addEventListener('click', _ddClick);
		}
		function _ddClick(event) {
			if (!_dropdown.has(event.target).length) {
				hideOptions()
			}
		}

		_dropdown.SetValue = function(idx, suppressEvents) {
			_value = items[idx]
			selectedIdx = idx
			kbDropdownValue.text(_value.Text || _value.Value)

			if (activeClass) {
				kbDropdownValue.removeClass(activeClass)
				activeClass = null
			}
			if (_value.Class) {
				kbDropdownValue.addClass(_value.Class)
				activeClass = _value.Class
			}
			if (!suppressEvents && onChange.length > 0) {
				onChange.forEach(f => f(idx, _value))
			}
		}

		_dropdown.OnChange = function(handler) {
			if (typeof handler == 'function') onChange.push(handler)
		}

		if (selectedIdx > -1) {
			_dropdown.SetValue(selectedIdx, true)
		}

		Object.defineProperty(_dropdown, 'Value', {
			get: function() { return _value.Value }
			, set: function(newValue) {
				var idx = items.findIndex(x => x.Value == newValue)
				_dropdown.SetValue(idx)
			}
		});

		Object.defineProperty(_dropdown, 'RawValue', {
			get: function() { return _value }
		});
	}
	return this
};

function KBDialog(options) {
	var centered = false
	var _title = ""
	var titleClose = true
	var hasButtons = false
	var hasCustomClasses = false
	var contentElement = null
	var beforeShow = []

	if (options != null) {
		for (var prop in options) {
			if (options.hasOwnProperty(prop)) {
				switch (prop) {
					case 'centered':
						centered = options.centered === true
						break
					case 'id':
						if (typeof options.id == 'string') {
							this.id = options.id
						} else {
							console.warn(this.id + " options : the given ID is not a string")
						}
						break;
					case 'title':
					case 'caption':
						if (typeof options[prop] == 'string') {
							_title = options[prop]
						} else {
							console.warn(this.id + " options : the given title is not a string")
						}
						break;
					case 'buttons':
						if (typeof options.buttons == 'object' && options.buttons.hasOwnProperty("length") && options.buttons.length > 0) {
							hasButtons = true
						}
						break
					case 'classes':
						if (typeof options.classes == 'object' && options.classes.hasOwnProperty("length") && options.classes.length > 0) {
							hasCustomClasses = true
						}
						break
					case 'titleClose':
					case 'closeBtn':
						titleClose = options[prop] === true
						break
					case 'content':
					case 'contentElement':
						var opt = options[prop]
						if (typeof opt == 'object') {
							if (opt instanceof jQuery) {
								contentElement = opt
							} else if (opt instanceof HTMLElement) {
								contentElement = $(opt)
							}
						} else if (typeof opt == 'string') {
							contentElement = $(opt)
						}
						break
					default:
						console.warn(this.id + " options : unknown option key: " + prop)
						break
				}
			}
		}
	}
	this.BeforeShow = function(handler) {
		if (typeof handler == 'function') beforeShow.push(handler)
	}
	if (!this.hasOwnProperty('id')) {
		this.id = 'kbDialog' + getRandomInt(100000, 999999)
	}

	Object.defineProperty(this, 'body', {
		get: function() { return dialogBody }
	});

	this.show = function(options) {
		var self = this
		if (beforeShow.length > 0) {
			beforeShow.forEach(f => f(self))
		}
		dialogWindow.modal(options)
	}
	this.hide = this.close = function() {
		dialogWindow.modal('hide')
	}
	this.$ = function(query) {
		return dialogBody.find(query)
	}

	var i = 0
	var dialogTitle = this.id + 'Title'
	var dialogWindow = $(document.body.appendTag('div'))

	dialogWindow.addClass('modal fade').attr({
		id: this.id
		, tabindex: -1
		, role: 'dialog'
		, 'aria-labelledby': dialogTitle
		, 'aria-hidden': 'true'
	})

	var dialogDocument = $('<div class="modal-dialog" role="document">').appendTo(dialogWindow)
	if (centered) {
		dialogDocument.addClass('modal-dialog-centered')
	}
	if (hasCustomClasses) {
		for (i = 0; i < options.classes.length; i++) {
			dialogDocument.addClass(options.classes[i])
		}
	}

	var dialogContent = $('<div class="modal-content">').appendTo(dialogDocument)

	var dialogHeader = $('<div class="modal-header">').appendTo(dialogContent)

	var dialogTitle = $('<h5 class="modal-title">').appendTo(dialogHeader).attr('id', dialogTitle).text(_title)

	if (titleClose) {
		dialogHeader.append($('<button class="close" type="button" data-dismiss="modal" aria-label="Закрыть">&times;</button>')) //<span class="close" aria-hidden="true"></span>
	}

	this.setTitle = function(title) {
		if (typeof title == 'string') {
			dialogTitle.text(title)
			_title = title
		}
		else {
			console.warn(this.id + ".setTitle : the given title is not a string")
		}
	}

	this.setText = function(text) {
		if (typeof text == 'string') {
			dialogBody.text(text)
		}
		else {
			console.warn(this.id + ".setTitle : the given text is not a string")
		}
	}

	this.setContent = function(element) {
		if (typeof element == 'string') {
			element = $(element)
		}
		if (element.hasClass('kb-content-holder')) {
			dialogBody.append(element.contents())
		} else {
			dialogBody.append(element)
		}
	}

	var dialogBody = $('<div class="modal-body">').appendTo(dialogContent)

	if (contentElement) {
		this.setContent(contentElement)
	}

	if (hasButtons) {
		var dialogFooter = $('<div class="modal-footer">').appendTo(dialogContent)

		var btnNum = 0;
		for (i = 0; i < options.buttons.length; i++) {
			var buttonData = options.buttons[i]
			if (typeof buttonData == 'object') {
				var dialogButton = dialogFooter[0].appendTag('button')
				dialogButton.classList.add('btn', 'btn-light')
				dialogButton.setAttribute('type', 'button')

				for (var prop in buttonData) {
					if (buttonData.hasOwnProperty(prop)) {
						switch (prop) {
							case 'text':
							case 'label':
							case 'caption':
								if (typeof buttonData[prop] == 'string') {
									dialogButton.appendText(buttonData[prop])
								} else {
									console.warn(this.id + " add button : the given button label is not a string")
								}
								break
							case 'onclick':
							case 'action':
								var action = buttonData[prop];
								if (typeof action == 'string') {
									if (action == 'close') {
										dialogButton.setAttribute('data-dismiss', 'modal')
									} else {
										console.warn(this.id + " add button : unknown button string action: \"" + action + "\"")
									}
								} else if (typeof action == 'function') {
									dialogButton.onclick = action
								} else {
									console.warn(this.id + " add button : the given button string action is neither a string nor a function")
								}
								break;
							case 'title':
							case 'hint':
								if (typeof buttonData[prop] == 'string') {
									dialogButton.setAttribute('title', buttonData[prop])
								} else {
									console.warn(this.id + " add button : the given button title is not a string")
								}
								break;
							case 'secondary':
							case 'grey':
								if (buttonData[prop] === true) {
									dialogButton.classList.remove('btn-light')
									dialogButton.classList.add('btn-secondary')
								}
								break;
							case 'style':
								if (typeof buttonData[prop] == 'string') {
									dialogButton.classList.remove('btn-light')
									dialogButton.classList.add('btn-' + buttonData[prop])
								}
								break;
						}
					}
				}
			} else if (typeof buttonData == 'string') {
				$('<button type=button data-dismiss=modal class="btn btn-primary">').appendTo(dialogFooter).text(buttonData)
			}
		}
	}
}

function initializeImageOverlay() {
	kbImageOverlay.controls = {
		element: BODY.appendTag('table')
		, prepare: function() {
			this.element.classList.add('ImageOverlayControls')
			this.element.setAttribute('cellpadding', '0')
			this.element.setAttribute('cellspacing', '0')
			this.row1 = this.element.appendTag('tr')
			this.row2 = this.element.appendTag('tr')
			this.row3 = this.element.appendTag('tr')
			this.row4 = this.element.appendTag('tr')
			this.row5 = this.element.appendTag('tr')
			this.scalingBtn = this.row1.appendTag('td')
			this.row1col1 = this.row1.appendTag('td')
			this.row1col2 = this.row1.appendTag('td')
			this.row1col3 = this.row1.appendTag('td')
			this.closeBtn = this.row1.appendTag('td')
			this.row2col = this.row2.appendTag('td')
			this.left = this.row3.appendTag('td')
			this.centre = this.row3.appendTag('td')
			this.right = this.row3.appendTag('td')
			this.row4col = this.row4.appendTag('td')
			this.row5col = this.row5.appendTag('td')

			this.scalingBtn.classList.add('scalingBtn')
			this.row1col1.classList.add('row1col1')
			this.row1col2.classList.add('row1col2')
			this.row1col3.classList.add('row1col3')
			this.closeBtn.classList.add('closeBtn')
			this.row2col.setAttribute('colspan', '5')
			this.row4col.setAttribute('colspan', '5')
			this.row5col.setAttribute('colspan', '5')
			this.row2col.classList.add('row2')
			this.row4col.classList.add('row4')
			this.row5col.classList.add('row5')
			this.left.setAttribute('colspan', '2')
			this.right.setAttribute('colspan', '2')
			this.left.classList.add('left')
			this.right.classList.add('right')

			this.description = this.row5col.appendTag('span')
			this.description.classList.add('description')
			this.scalingBtn.naturalSize = false

			this.left.onclick = function() {
				kbImageOverlay.carousel.slideLeft()
			}
			this.right.onclick = function() {
				kbImageOverlay.carousel.slideRight()
			}
			this.closeBtn.onclick = function() {
				$(kbImageOverlay.controls.element).fadeOut(150)
				$(kbImageOverlay.carousel.active).fadeOut(150)
			}
			this.scalingBtn.onclick = function() {
				var c = kbImageOverlay.carousel
				this.naturalSize = !this.naturalSize
				if (this.naturalSize) {
					c.first.classList.add('naturalSize')
					c.second.classList.add('naturalSize')
					this.classList.add('naturalSize')
				} else {
					c.first.classList.remove('naturalSize')
					c.second.classList.remove('naturalSize')
					this.classList.remove('naturalSize')
				}
			}
			this.setDescription = function(descr) {
				if ((descr == null) || (typeof descr != 'string') || (descr.length == 0)) {
					this.description.style.display = 'none'
				} else {
					this.description.style.display = ''
					$(this.description).text(descr)
				}
			}
		}
	}
	kbImageOverlay.carousel = {
		first: BODY.appendTag('div')
		, second: BODY.appendTag('div')
		, prepare: function() {
			function makeImg(element) {
				element.classList.add('carouselDiv')
				element.classList.add('transitive')
				element.imageWrapper = element.appendTag('div')
				element.imageWrapper.classList.add('imageWrapper')
				element.image = element.imageWrapper.appendTag('img')
				element.image.classList.add('carouselImage')
			}
			makeImg(this.first)
			makeImg(this.second)
			this.second.style.display = 'block';
			this.active = this.first
			this.inactive = this.second
			this.inactive.style.left = '100%'
			this.data = kbImageOverlay.images
			this.current = {idx: 0, set: null}
		}
		, slideLeft: function() {
			this.current.idx--
			if (this.current.idx < 0) {
				this.current.idx = this.current.set.length - 1
			}
			this.inactive.remove()
			this.inactive.style.left = '-100%'
			var imgdata = this.current.set[this.current.idx]
			this.inactive.image.setAttribute('src', imgdata.src)
			kbImageOverlay.controls.setDescription(imgdata.descr)
			var ac = this.active
			this.active = this.inactive
			this.inactive = ac
			BODY.appendChild(this.active)
			setTimeout(function() {
				kbImageOverlay.carousel.inactive.style.left = '100%'
				kbImageOverlay.carousel.active.style.left = '0px'
			}, 5);
		}
		, slideRight: function() {
			this.current.idx++
			if (this.current.idx == this.current.set.length) {
				this.current.idx = 0
			}
			this.inactive.remove()
			this.inactive.style.left = '100%'
			var imgdata = this.current.set[this.current.idx]
			this.inactive.image.setAttribute('src', imgdata.src)
			kbImageOverlay.controls.setDescription(imgdata.descr)
			var ac = this.active
			this.active = this.inactive
			this.inactive = ac
			BODY.appendChild(this.active)
			setTimeout(function() {
				kbImageOverlay.carousel.inactive.style.left = '-100%'
				kbImageOverlay.carousel.active.style.left = '0px'
			}, 5);
		}
	}
	kbImageOverlay.images = {}
	kbImageOverlay.controls.prepare()
	kbImageOverlay.carousel.prepare()
	
	kbImageOverlay.registerImage = function(image) {
		var imgSrc = false;
		if (image == undefined) {
			console.error('addImage: image is undefined')
			return
		} else if (!((typeof image == 'object') && image instanceof HTMLElement)) {
			console.error('addImage: image is not an HTML element')
			return
		}
		if (image.tagName == 'IMG') {
			imgSrc = image.getAttribute('src')
		} else {
			imgSrc = image.style.backgroundImage
			if (imgSrc && imgSrc.length > 4) imgSrc = imgSrc.split('"')[1]
		}
		if (!imgSrc) {
			console.error('addImage: the element doesn\'t contain an image')
			return
		}

		var setname = image.getAttribute('kbSet')
		if (!setname) {
			var setname = image.getAttribute('kbImageSet')
			if (!setname) {
				console.error('addImage: kbSet is not defined for the image')
				return
			}
		}
		if (!this.images.hasOwnProperty(setname)) {
			this.images[setname] = []
		}
		this.images[setname].push({src: imgSrc, descr: image.getAttribute('title')})
		image.imgOverlayIdx = this.images[setname].length - 1
		image.imageSetName = setname
		image.onclick = function() {
			kbImageOverlay.showImage(this.imageSetName, this.imgOverlayIdx)
		}
	}

	kbImageOverlay.refreshData = function() {
		this.images = { _SINGLE: {src: null, descr: ''}}
		$('img[kbSet]').each(function() {
			kbImageOverlay.registerImage(this);
		});
		$('[kbImageSet]').each(function() {
			kbImageOverlay.registerImage(this);
		});
		this.carousel.data = this.images
	}

	kbImageOverlay.showImage = function(setname, idx) {
		if (typeof setname == 'object' && setname instanceof HTMLElement) {
			var imgSrc = null
			if (setname.tagName == 'IMG') {
				imgSrc = setname.getAttribute('src')
			} else {
				throw new Exception('kbImageOverlay.showImage: setname is not an image element')
			}
			var current = kbImageOverlay.carousel.current
			current.set = kbImageOverlay.images._SINGLE
			current.set.src = imgSrc
			current.idx = 0
			this.carousel.active.image.setAttribute('src', imgSrc)
			this.controls.setDescription('')
		} else if (typeof setname == 'string') {
			var current = this.carousel.current
			current.set = this.images[setname]
			current.idx = idx
			var imgdata = current.set[current.idx]
			this.carousel.active.image.setAttribute('src', imgdata.src)
			this.controls.setDescription(imgdata.descr)
		} else {
			throw new Exception('kbImageOverlay.showImage: setname is not a string')
		}
		$(this.controls.element).fadeIn(150)
		$(this.carousel.active).fadeIn(150)
	}

	kbImageOverlay.refreshData()
	kbImageOverlay.initialized = true
}

function makeSpoiler(hider) {
	hider = validateElement(hider)
	if (!hider) {
		console.error('makeSpoiler: given hider is not an element or id')
		return
	}
	if (!hider.hasAttribute('kbSpoiler')) {
		console.error('makeSpoiler: given hider has no kbSpoiler attribute')
		return
	}
	hider.spoiler = validateElement(hider.getAttribute('kbSpoiler'))
	if (!hider.spoiler) {
		console.error('makeSpoiler: given hidden is not an element or id')
		return
	}
	hider.plusminus = hider.prependTag('span')
	$(hider.plusminus).text('[+] ')
	hider.classList.add('kbSpoiler')
	hider.spoiler.classList.add('kbSpoilerContent')
	hider.plusminus.classList.add('plusminus')
	hider.onclick = function() {
		var plusminustext = this.spoiler.style.display == 'none' ? '[-] ' : '[+] '
		$(this.spoiler).slideToggle(300)
		$(this.plusminus).text(plusminustext)
	}
}

function kbGetOffsetRect(elem) {
	var box = elem.getBoundingClientRect();
	var scrollTop = window.pageYOffset;
	var scrollLeft = window.pageXOffset;
	var top  = box.top +  scrollTop;
	var left = box.left + scrollLeft;
	return {
		top: Math.round(top) ? Math.round(top) : "0"
		, left: Math.round(left) ? Math.round(left) : "0"
	}
}

function kbPreviewImg(setname, src, descr) {
	var width = arguments.length > 3 ? arguments[3] : 160;
	document.write('<img kbSet="' + setname + '" src="' + src + '" class="previewImg" title="' + descr + '" style="width:' + width + 'px;">');
}

function showMessageBox(text, level) {
	if (typeof text == 'string') {
		kbMessageBox.setText(text)
		if (typeof level == 'string') {
			switch (level) {
				case 'error':
					kbMessageBox.setTitle('Ошибка')
					break
				case 'warn':
					kbMessageBox.setTitle('Внимание')
					break
				default:
					kbMessageBox.setTitle(level)
					break
			}
		} else {
			kbMessageBox.setTitle('Внимание')
		}
	} else if (typeof text == 'object' && text.Message) {
		kbMessageBox.setText(text.Message)
		if (text.Title) {
			kbMessageBox.setTitle(text.Title)
		}
	}
	kbMessageBox.show()
}